function neg_img_array = get_negatives_from_positive_train_set(input_folder, goal_rect_size, max_img_number)
%This function cuts non-face images with predefined 
%size from a folder of images with ground truth.
%
% Inputs:
%       input_folder    - path to the input image folder
%       goal_rect_size  - size of the output rectangular 
%                         shaped negative images
%   ### max_img_number - setting numbert of images
%
% Output:
%       neg_img_array   - a cell array of non face images



%seed the random generator to generate the same set of random 
%images for each run, so the process remains deterministic.
rng_seed = 13;
if(~isempty(rng_seed))
    saved_rng = rng();
    rng(rng_seed);
end

%generate the list of available images in the input folder
list_of_images = rdir(path_corrector([input_folder '\**\*.jpg']));
neg_img_array = {};
c = 0;
%overlap threshold: it sets the maximum allowed overlap between a negative 
%sample and a ground truth face rectangle.
overlap_th = 0.3; 

%### ensuring max_img_number: ...
%### we don't want to load not existing images
if max_img_number >= length(list_of_images)
    max_img_number = length(list_of_images);
end
%###

%### replacing length(list_of_images) with max_img_number
for i = 1:max_img_number
    img_path = list_of_images(i).name;
    %if the image exists
    if exist(img_path ,'file')
        %read in the image
        img = imread(img_path);
        im_size = size(img);
        
        %read face data
        data = load([img_path(1:end-3) 'mat']);
        nr_faces = size(data.faces,1);
        
        %create random negative rects
        %#### tobb negativ teglalap generalasa
        nr_rand_rects = 10; %#random rects created per image
        rand_rects = zeros(nr_rand_rects,4);
        rect_limits = [min(data.faces(:,3))*0.25, max(data.faces(:,3))*2];
        for f = 1:nr_rand_rects
            c_rect_size = round(rand(1)*(rect_limits(2)-rect_limits(1)) + rect_limits(1));
            c_rect_size = min([c_rect_size, im_size([2,1])-1]);
            c_rect_size = max([c_rect_size, goal_rect_size]);
            
            %get random position
            random_pos = rand(1,2);
            top_pt = floor(random_pos.*(im_size([2,1])-(c_rect_size+1)))+1;
            rand_rects(f, :) = [top_pt, c_rect_size, c_rect_size];
        end
         
        %Filter rects with significant intersect with a face. It is necessary
        %to avoid accidental use of face bounding boxes in the negative set. 
        inters_ratio = zeros(nr_faces, nr_rand_rects);
        for f = 1:nr_faces
            inters_ratio(f,:) = face_classifier_t.calc_intersection_ratio(data.faces(f,:), rand_rects);
        end
        keepers = max(inters_ratio,[], 1) < overlap_th; 
        rand_rects = rand_rects(keepers,:);
        nr_kepers = sum(keepers);
        
        
%         %visualize kept negative samples
%         %create filter for face areas
%         face_filter = zeros(im_size(1:2));
%         for f = 1:nr_faces
%             face_filter(data.faces(f,2) : data.faces(f,2) + data.faces(f,4),...
%                         data.faces(f,1) : data.faces(f,1) + data.faces(f,3)) = 1;
%         end
%         for f = 1:nr_rand_rects
%             inters_ratio(f) 
%             neg_filter = face_filter;
%             neg_filter(rand_rects(f,2) : rand_rects(f,2) + rand_rects(f,4),...
%                         rand_rects(f,1) : rand_rects(f,1) + rand_rects(f,3)) = 0.5;
%             figure; imshow(neg_filter);
%             waitforbuttonpress;
%             close all;
%         end

        data.faces = int32(data.faces);
        for f = 1:nr_kepers
            neg_img = img(rand_rects(f,2) : rand_rects(f,2) + rand_rects(f,4),...
                               rand_rects(f,1) : rand_rects(f,1) + rand_rects(f,3),:);          
            %resize images to a given common size
            fi_size = size(neg_img);
            res_ratio = goal_rect_size/min(fi_size(1:2));
            neg_img = imresize(neg_img, res_ratio);
            
            %put the image into an array
            c = c+1;
            neg_img_array{c} = neg_img;
        end
    end
end

%after the seeded random number generation, we reload the original state of
%the random number generator. (see line 15-21)
if(~isempty(rng_seed))
    rng(saved_rng);
end

