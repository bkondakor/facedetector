function pos_img_array = read_positive_train_set(input_folder, goal_rect_size, max_img_number)
%This function cuts the face images with predefined 
%size from a folder of images with ground truth.
%
% Inputs:
%       input_folder    - path to the input image folder
%       goal_rect_size  - size of the output rectangular shaped face images
%   ### max_img_number  - max number of trainig images - to reduce testing time  
% Output:
%       pos_img_array   - a cell array of face images



%generate the list of available images in the input folder
list_of_images = rdir(path_corrector([input_folder '\**\*.jpg']));

pos_img_array = {};
c = 0;

%### ensuring max_img_number: ...
%### we don't want to load not existing images
if max_img_number >= length(list_of_images)
    max_img_number = length(list_of_images);
end
%###

%### replacing length(list_of_images) to max_img_number
for i = 1:max_img_number
    %get the path of the current image
    img_path = list_of_images(i).name;
    
    %if the image exists
    if exist(img_path ,'file')
        %read in the image
        img = imread(img_path);
        im_size = size(img);
        
        %read face ground truth data
        data = load([img_path(1:end-3) 'mat']);
        nr_faces = size(data.faces,1);
        
        %padding the image if necessary
        min_runoff = ceil(1 - min(data.faces(:)));
        max_runoff = ceil(max(data.faces(:, 1:2) + ...
                          data.faces(:, 3:4), [], 1)-im_size([2,1]));
        %length of the running off or zero if it is negative
        pad_size = max([min_runoff, max_runoff, 0])+1; 
        if pad_size > 0
            pad_img = padarray(img, [pad_size, pad_size]);
            data.faces(:,1:2) = data.faces(:,1:2) + pad_size;
        else
            pad_img = img;
        end

        data.faces = int32(data.faces);
        for f = 1:nr_faces
            face_img = pad_img(data.faces(f,2):data.faces(f,2)+data.faces(f,4),...
                               data.faces(f,1):data.faces(f,1)+data.faces(f,3),:);          
            %resize images to a given common size
            fi_size = size(face_img);
            res_ratio = goal_rect_size/min(fi_size(1:2));
            face_img = imresize(face_img, res_ratio);
            
            %put the image into an array
            c = c+1;
            pos_img_array{c} = face_img;
%             %visualize
%             figure; imshow(face_img);
%             waitforbuttonpress; close all;
        end
    end
end

