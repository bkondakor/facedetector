%classifier
%%% Initialization
clear all; close all; clc 
%### add src folder to path
addpath(genpath(fileparts(mfilename('fullpath'))));
team_id = 11;%DON'T FORGET TO CHANGE THIS

%%% initialize input and output folders (Check these before you execute!)
%### Setting size of train, validation and test sets
train_num = 1100;
validation_num = 100000;
test_num = 100000;

%###treshold feljebballitasa
det_th = 0.99; 


disp('initializing');
time_tag = datestr(now, 'mm_dd_HH_MM');
base_folder = path_corrector([fileparts(mfilename('fullpath')) '\..\']);
train_img_folder = path_corrector([base_folder '\data\positive_samples\train\']);
test_img_folder = path_corrector([base_folder '\data\positive_samples\test\']);
validation_img_folder = path_corrector([base_folder '\data\positive_samples\validation\']);


%%% load positive and negative training samples
goal_rect_size = 52; %face images will be cut and resized to [goal_rect_size x goal_rect_size]
%get positive sample images
%### get exact number of images instead of all
%###
disp('reading positive');
pos_train_img_array = read_positive_train_set(train_img_folder, goal_rect_size, train_num);

% pos_train_img_array
% get random negative sample images from positive database
%###
disp('getting negative images');
%### get exact number of images instead of all
neg_img_array = get_negatives_from_positive_train_set(train_img_folder, goal_rect_size, train_num);
%%% load positive and negative training samples


%%% initialize classifier object
%###
disp('intializing classifier');
% classifier = logit_classifier_t([goal_rect_size, goal_rect_size]);
classifier = svm_classifier([goal_rect_size, goal_rect_size]);

%%% initialize classifier object

%%% create training samples and labels
%create feature vector for each sample
%###
disp('creating feature vectors(training samples and labels)');
tr_samples = classifier.imgs2samples([neg_img_array, pos_train_img_array]);


nr_pos = length(pos_train_img_array);
nr_neg = length(neg_img_array);
%zero labels for negative samples and one labels for positive samples
tr_labels = [zeros(nr_neg, 1); ones(nr_pos, 1)];
tr_labels(all(tr_samples==0,2),:) = [];
tr_samples(all(tr_samples==0,2),:) = [];
clear neg_img_array pos_train_img_array %clear unused variables to free the memory
%%% create training samples and labels


%%% Train classifier
%###
disp('train classifier');
classifier.train(tr_samples, tr_labels);

%set the folder where the trained classifier will be saved
cl_output_folder = path_corrector([base_folder '/output/trained_classifiers/']);
if ~exist(cl_output_folder, 'dir')
    mkdir(cl_output_folder);
end
cl_filepath = path_corrector([cl_output_folder '/cl_model_' time_tag '.mat']);
save(cl_filepath, 'classifier'); %save the trained classifier model
disp('training');
%%% Train classifier
    

%set detection threshold: this value controls how "conservative" our classifier is. 
%If it is set to 0.5, then the sample will be classified as face only if the
%classifier gives >0.5 probability to it to be a face. Yoc can use this
%threshold to set the balance between true positive and false positive hits.


%%Processing of the validation set

%###
disp('processing validation');

% set the folder for the evaluation output
out_val_img_folder = path_corrector([base_folder '/output/validation/' time_tag '/']);
%### get exact number of images instead of all
detection_on_validation = process_image_set(classifier, validation_img_folder, out_val_img_folder, det_th, validation_num);    
validation_results = evaluation(detection_on_validation, validation_img_folder);
fprintf('\nWith %0.2f detection threshold the F0.5-Score on the validation set is %0.4f\n', det_th, validation_results.f_score);
%%Processing of the validation set

%%Processing of the test set
%###
disp('processing test set');
% set the folder for the evaluation output
out_test_img_folder = path_corrector([base_folder '/output/on_test_set/' time_tag '/']);
%### get exact number of images instead of all
detection_on_testset = process_image_set(classifier, test_img_folder, out_test_img_folder, det_th, test_num);    
output_folder = path_corrector([base_folder '/output/on_test_set/']);
save_results_as_text(detection_on_testset, team_id, output_folder, time_tag);
%%Processing of the test set
