function out_img = plot_rect_on_img(img, rect, rect_color)
%this function draws a rectangle on the image.
%
% Inputs:
%       img         - input image
%       rect        - rectangle defined by its top left 
%                       corner and its width and height
%       rect_color  - color of the rectangle
% Output:
%       out_img     - image with the rectangle

if ~exist('rect_color', 'var')
    rect_color = 255;
end

out_img = img;
out_img(rect(2):rect(2)+rect(4), rect(1), :)        = rect_color;
out_img(rect(2):rect(2)+rect(4), rect(1)+rect(3),:) = rect_color;
out_img(rect(2), rect(1):rect(1)+rect(3),:)         = rect_color;
out_img(rect(2)+rect(4), rect(1):rect(1)+rect(3),:) = rect_color;
