classdef face_classifier_t < handle
%Abstract class for face classification.

    properties (Access = public)
        %working size of the face candidate image samples
        face_size = [48,48]; 
        
        %set the minimum face size we want to detect 
        %(a face smaller than the im_size*min_face_ratio won't be detected)
        min_face_ratio = 1/6;
        
        %set the resize ratio between two level of the image pyramid
        resize_increase = 1.3;
        
        %sets the overlap ratio between two neighburing candidate bounding boxes 
        overlap_ratio = 0.9;
        
        %intersecting detections will be combined if the intersection is
        %greater than this threshold
        combin_inters_th = 0.01;
    end
    
    
    %Abstract methods of the class. These have 
    %to be implemented in the derived classes.
    methods(Abstract)
        %this method trains the classifier based on 
        %samples and corresponding ground truth labels
        train(this, samples, labels); 
        
        %this method predicts the label of the input samples (i.e. face or not face)
        values = predict(this, samples);
    end
    
    
    methods
        function [face_bboxs] = detect_face_on_img(this, img, th)
            %This function detects faces on the input image with the trained classifier
            %object. If the classifier produce probability output, the threshold can
            %also be set.
            %
            %   Inputs:
            %       img         - input image
            %       th          - probability threshold (those samples that produce a 
            %                     probability below the threhold will be classified as 
            %                     not face)
            %   Output:
            %       face_bboxs  - bounding boxs of the found faces 
            %                       ([x, y, w, h] where (x,y) are the coordinates of
            %                       the top left corner of the bounding box, while 
            %                       w and h are the width and height. Sinc we work
            %                       with square shaped faces: w=h.) 


            %convert the image to grayscale
            im_size = size(img);
            if size(img,3) == 3
                img = rgb2gray(img);
            end

            f_size = this.face_size;
            min_res_ratio = f_size(1)/min(im_size(1:2));
            max_res_ratio = f_size(1)*(1/this.min_face_ratio)/min(im_size(1:2));

            face_bboxs = [];
            cntr = 0;
            rr = min_res_ratio;
            while rr < max_res_ratio
                %resize the input image to the current level in the image pyramid
                r_img = imresize(img, rr);

                %generate overlapping rectangle shaped samples from the image, that a
                %classifier object an handle
                r_isize = size(r_img);
                ol = floor(f_size*this.overlap_ratio); %set the overlap of the rectangles
                [YY,XX] = ndgrid( 1:(f_size(1)-ol):(r_isize(1)-(f_size(1)-1)),...
                                  1:(f_size(2)-ol):(r_isize(2)-(f_size(2)-1)));
                cYY = num2cell(YY);
                cXX = num2cell(XX);
                C = cellfun(@(cYY,cXX) r_img(cYY+(0:f_size-1),cXX+(0:f_size-1)),...
                                                cYY, cXX, 'UniformOutput', false); 
                C = C(:);

                %extract features from the sample images
                output_samples = this.imgs2samples(C);
                %predict the label of the samples (face/not face)
                [pred_labels, pred_probs] = this.predict(output_samples); 


                %get the index of samples classified as face
                if exist('th', 'var')
                    ff_idx = find(pred_probs> th);
                else
                    ff_idx = find(pred_labels);
                end

                %generate bounding box (on the normal sized image) for the samples
                %classified as face
                for f = 1:length(ff_idx) 
                    cntr = cntr+1;
                    face_bboxs(cntr, :) = round([XX(ff_idx(f)), YY(ff_idx(f)),...
                                            f_size(1), f_size(2)]*(1/rr));
                    face_probs(cntr) = pred_probs(ff_idx(f));
                end

                %calculate resize ratio for the next level of the image pyramid
                rr = rr*this.resize_increase;
            end

            %combine the detected face bounding boxes: keep only one from the
            %significantly overlapping bounding boxes
            if ~isempty(face_bboxs)
                face_bboxs = this.combine_hits(face_bboxs, face_probs, this.combin_inters_th);
            end
        end
    end
    
    
    methods (Static)
        function new_rects = combine_hits(rects, probs, combin_inters_th)
            %Combines overlapping bounding boxes if their intersection is
            %above a threshold.
            %
            %   Inputs: 
            %       rects - Input bounding boxes
            %       probs - The probabilities that a rectangle (in "rects")
            %               is containing a face (i.e. the face classifier's 
            %               probability output for the rectangle). 
            %       combin_inters_th - The intersection threshold, above
            %               which the overlapping rectangles will be 
            %               combined: the rectangle with the higher probability
            %               will be kept, while the other eliminated.
            %
            %   Output:
            %       new_rects - A new set of rectangles, without
            %                   significant (== above the threshold) overlap 
            

            inters_ratio = zeros(size(rects, 1),size(rects, 1));
            for i = 1:size(rects, 1)
                inters_ratio(i,:) = face_classifier_t.calc_intersection_ratio(rects(i,:), rects);
            end
            inters_ratio = triu(inters_ratio, 1);
            inters_ratio = inters_ratio + inters_ratio';

            [~, sorted_idxs] = sort(probs, 'descend');
            all_to_elim = [];
            for i = 1:length(sorted_idxs)
                to_elim = find(inters_ratio(sorted_idxs(i), :) > combin_inters_th);
                inters_ratio(:, to_elim) = 0;
                inters_ratio(to_elim, :) = 0;
                all_to_elim = [all_to_elim, to_elim];
            end

            new_rects = rects;
            new_rects(all_to_elim, :) = []; 
        end
        
        
        function int_rate = calc_intersection_ratio(rect1, all_rects)
            %Calculate the intersection of one rect with an array of rects.
            %   Inputs:
            %       rect1 - a rectangle
            %       all_rects - an array of rectangles
            %
            %   Output:
            %       int_rate - a vector with the intersection ratios between 
            %                   'rect1' and the rectangles in 'all_rects'
            %
            
            p1_top = rect1([1,2]);
            p1_bot = p1_top + rect1([3,4]);
            p2_top = all_rects(:, [1,2]);
            p2_bot = p2_top + all_rects(:, [3,4]);

            pi_top = [max(p2_top(:, 1), p1_top(1)), max(p2_top(:, 2), p1_top(2))];
            pi_bot = [min(p2_bot(:, 1), p1_bot(1)), min(p2_bot(:, 2), p1_bot(2))];

            int_rect_sizes = (pi_bot - pi_top);
            intersections = int_rect_sizes(:,1).*int_rect_sizes(:,2);
            %set the non intersecting rectangles intersection to zero
            %and also handles the [-,-] case
            intersections(sum(int_rect_sizes<0, 2)>0) =0; 

            union_size = (rect1(3)*rect1(4)) + (all_rects(:, 3).*all_rects(:, 4)) - intersections;
            int_rate = intersections./union_size;
        end
    end
    
end