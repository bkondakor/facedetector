function outpath = path_corrector(inpath)
%This function replaces all the path separator character with the one
%adequate for the give platform.

sep_idxs = strfind(inpath, '/');
sep_idxs = [sep_idxs, strfind(inpath, '\')];
outpath = inpath;
outpath(sep_idxs) = filesep;