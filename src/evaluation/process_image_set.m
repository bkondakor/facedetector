function detection_results = process_image_set(classifier, ...
                            in_img_folder, out_eval_img_folder, det_th, max_img_number)
% This function runs the face detection on a set of images, which are in
% the same folder.
% 
%   Inputs:
%       classifier - the trained face detector class
%       in_img_folder - the folder containing images on which we want to
%                           detect faces
%       out_eval_img_folder - output folder, where the input images with 
%                               the detected face rectangles are visualized
%       det_th - detection threshold for the classifier
%   ### max_img_number - max number of processed images
%
%   Output:
%       detection_results - a structure with the detection results

                        
if ~exist(out_eval_img_folder, 'dir')
    mkdir(out_eval_img_folder);
end                        
%%%Processing of the test set
list_of_test_imgs = rdir(path_corrector([in_img_folder '/**/*.jpg']));
nr_test_imgs = length(list_of_test_imgs);

%### ensuring max_img_number: ...
%### we don't want to load not existing images
if max_img_number >= nr_test_imgs
    max_img_number = nr_test_imgs;
end
%###

detection_results = [];

%### replacing nr_test_imgs with max_img_number
for i = 1:max_img_number
    c_test_img_path = list_of_test_imgs(i).name;
    img = imread(c_test_img_path);
    est_faces = classifier.detect_face_on_img(img, det_th);

    %generate image with the detected faces
    img_with_faces = img;
    for f = 1:size(est_faces,1)
        img_with_faces = plot_rect_on_img(img_with_faces, est_faces(f,:));
    end
    %generate image with the detected faces

    %DO NOT CHANGE THE STRUCTURE OF THE EVALUATION RESULT%
    %save a struct with candidate faces and image file names
    [~, img_name] = fileparts(c_test_img_path); 
    detection_results(i).img_name = img_name;
    detection_results(i).cand_faces = est_faces;
    

    [~, file_name] = fileparts(c_test_img_path);
    imwrite(img_with_faces, [out_eval_img_folder file_name '_test.jpg']);
end
%%%Processing of the test set