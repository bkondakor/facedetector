classdef svm_classifier < face_classifier_t
%Face classifier class, that uses logistic regression for classification. 
%This class is derived from the abstract class of face_classifier_t.  
    
    properties (Access = public)
        model = []; %instance variable for the trained classifer model
    end

    
    methods
        function this = svm_classifier(face_size, trained_model)
            %Constructor of the class logit_classifier_t
            if ~isempty(face_size)
                this.face_size = face_size;
            end
            
        end    
        
        function output_samples = imgs2samples(this, face_imgs)
            %This function executes the feature extraction 
            %on the input image database.
            nr_samples = length(face_imgs);
            sample_size = numel(this.extract_features(face_imgs{1}));
            output_samples = zeros(nr_samples, sample_size, 'single');
            
            for i = 1: nr_samples
                extracted_features = this.extract_features(face_imgs{i});
                if(length(extracted_features(:)) > 0)
                    output_samples(i, :) = extracted_features(:);
                end
            end
            
        end
        
        function features = extract_features(this, in_img)
            %This function extract features from the sample image. In this
            %case the extracted features are just the pixel intensities. 
            
            %resize the image to uniform size if necessary
            if this.face_size ~= size(in_img,1)
                p_img = imresize(in_img, this.face_size);
            else
                p_img = in_img;
            end

            %convert it to grayscale
            if size(p_img, 3) == 3
                p_img = rgb2gray(p_img);
            end

            %extract features: use pixel intensity values
            %features = single(p_img(:));
            
            %### Extracting features
            
            % extracting HOG-features
            % with cell size: 32, 16 and 8
            %trhog32 = vl_hog(im2single(p_img), 32);
            trhog16 = vl_hog(im2single(p_img), 16);
            trhog8 = vl_hog(im2single(p_img), 8);

            % Extracting LBP-features
            % with cell size: 32, 16 and 8
            %trlbp32 = vl_lbp(im2single(p_img), 32);
            trlbp16 = vl_lbp(im2single(p_img), 16);
            trlbp8 = vl_lbp(im2single(p_img), 8);
            
%             % Extracting SIFT-features
            [F,D] = vl_sift(single(p_img));
            % deleting images, where vl_sift found nothing
            if size(D, 2) >= 3;
                D = D(:,1:3);
                trsift = D;     
            else
                trsift = [];
            end


            % Constracting feature vector from calculated features
            % Creating 1D vectors from all type of features to make handling easier

            hog8vect = trhog8(:);
            lbp8vect = trlbp8(:);
            %siftvect = trsift(:);
            features = single([hog8vect', lbp8vect']);  %, siftvect];
%             features = single(hog8vect');  %, siftvect];
%             disp(num2str(length(features)))
            %###
            
        end

        function train(this, training_samples, training_labels)
            %Train the logistic regressor and store the model parameters in
            %the 'model' instance variable.
            
            nr_samples = length(training_labels);
            L_search = 500;
            disp('Grid search started')
            [C, gamma] = svm_classifier.grid_search(training_samples(1:round(nr_samples/L_search):end, :), training_labels(1:round(nr_samples/L_search):end));
            params = ['-s 0 -t 2 -q -h 1 -b 1 -c ', num2str(C), ' -g ', num2str(gamma)];
            disp('Train started')
            this.model = svmtrain(double(training_labels), double(training_samples), params);


        end
        

        
        function [pred_labels, pred_prob] = predict(this, samples)
            %This function predicts (estimates) the label of a previously
            %unseen sample. The predicted label is binary (face or not
            %face), the predicted probability is a continuous value between
            %0 and 1, the higher value means that the sample is more likely
            %to belong to a face. Depending on the classification method
            %this may or may not be a real probability.
           
            nr_samples = size(samples, 1);
            svm_options2 = ['-b 1 -q'];
            [pred_lab, acc, dec_val] = svmpredict(double(ones(nr_samples, 1)), double(samples), this.model, svm_options2);
            pred_prob = dec_val(:, 2);
            pred_labels = pred_prob >= 0.5;
            
        end
    end
    
    
    methods(Static)
        function [C, gamma] = grid_search(samples, labels)

        C = 1;
        gamma = 1;

        N = 7;

        acc_values = double(zeros(N));
        c = repmat(logspace(-2,4,N), N, 1);
        g = repmat(logspace(-5,1,N)', 1, N);
        parfor i = 1:N*N
                params = ['-s 0 -t 2 -q -h 1 -b 1 -v 5 -c ', num2str(c(i)), ' -g ', num2str(g(i))];
                acc_values(i) = svmtrain(double(labels), double(samples), params);
        end



        [i,j] = ind2sub(size(acc_values), find(acc_values==(max(max(acc_values)))));

        if(length(i) > 1)
        C = c(i(1), j(1));
        gamma = g(i(1), j(1));
        else
        C = c(i, j);
        gamma = g(i, j);    
        end

        end
    end
end