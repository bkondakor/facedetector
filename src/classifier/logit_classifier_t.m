classdef logit_classifier_t < face_classifier_t
%Face classifier class, that uses logistic regression for classification. 
%This class is derived from the abstract class of face_classifier_t.  
    
    properties (Access = public)
        model = []; %instance variable for the trained classifer model
    end

    
    methods
        function this = logit_classifier_t(face_size)
            %Constructor of the class logit_classifier_t
            if ~isempty(face_size)
                this.face_size = face_size;
            end 
        end    
        
        function output_samples = imgs2samples(this, face_imgs)
            %This function executes the feature extraction 
            %on the input image database.
            
            nr_samples = length(face_imgs);
            sample_size = numel(this.extract_features(face_imgs{1}));
            output_samples = zeros(nr_samples, sample_size, 'single');
            
            for i = 1: nr_samples                       
                extracted_features = this.extract_features(face_imgs{i});
                output_samples(i, :) = extracted_features(:);
            end
        end
        
        function features = extract_features(this, in_img)
            %This function extract features from the sample image. In this
            %case the extracted features are just the pixel intensities. 
            
            %resize the image to uniform size if necessary
            if this.face_size ~= size(in_img,1)
                p_img = imresize(in_img, this.face_size);
            else
                p_img = in_img;
            end

            %convert it to grayscale
            if size(p_img, 3) == 3
                p_img = rgb2gray(p_img);
            end

            %extract features: use pixel intensity values
            %features = single(p_img(:));
            
            %### Extracting features
            
            % extracting HOG-features
            % with cell size: 32, 16 and 8
            %trhog32 = vl_hog(im2single(p_img), 32);
            trhog16 = vl_hog(im2single(p_img), 16);
            trhog8 = vl_hog(im2single(p_img), 8);

            % Extracting LBP-features
            % with cell size: 32, 16 and 8
            %trlbp32 = vl_lbp(im2single(p_img), 32);
            trlbp16 = vl_lbp(im2single(p_img), 16);
            trlbp8 = vl_lbp(im2single(p_img), 8);

%             % Extracting SIFT-features
%             [F,D] = vl_sift(single(img));
%             % deleting images, where vl_sift found nothing
%             if size(D, 2) < 1;
%                  trsift(i,:) = [];
%             else
%                  D = D(:,1);
%                  trsift(i,:) = D;
%             end


            % Constracting feature vector from calculated features
            % Creating 1D vectors from all type of features to make handling easier

            hog8vect = trhog8(:);
            lbp8vect = trlbp8(:);
            %siftvect = trsift(:);
            features = single([hog8vect', lbp8vect']);  %, siftvect];
%             features = single(hog8vect');  %, siftvect];
%             disp(num2str(length(features)))
            %###
            
        end

        function train(this, training_samples, training_labels)
            %Train the logistic regressor and store the model parameters in
            %the 'model' instance variable.
            
            nr_samples = length(training_labels);
            training_samples = [ones(nr_samples, 1), training_samples];
            dim_of_features = size(training_samples, 2);
            initial_theta = zeros(dim_of_features, 1); 
            options = optimset('GradObj', 'on', 'MaxIter', 400);
            %  Run fminunc to obtain the optimal theta
            [theta, cost] = ...
                fminunc(@(t)(logit_classifier_t.cost_function(...
                              t, double(training_samples),...
                              double(training_labels))), initial_theta, options); 
            this.model.theta = theta;
        end
        

        
        function [pred_labels, pred_prob] = predict(this, samples)
            %This function predicts (estimates) the label of a previously
            %unseen sample. The predicted label is binary (face or not
            %face), the predicted probability is a continuous value between
            %0 and 1, the higher value means that the sample is more likely
            %to belong to a face. Depending on the classification method
            %this may or may not be a real probability.
           
            nr_samples = size(samples, 1);
            samples = [ones(nr_samples, 1), samples];
            pred_prob = logit_classifier_t.sigmoid(samples*this.model.theta);
            pred_labels = pred_prob>= 0.5; 
        end
    end
    
    
    methods(Static)
        function [J, grad] = cost_function(theta, X, labels)
            %This function compute cost and gradient for logistic regression

            m = length(labels); % number of training examples           
            h = 1./(1+exp(-(X*theta)));
            J = (1/m)*sum( -labels.*log(h) - (1-labels).*log(1-h) );
            
            grad = zeros(size(theta));
            for t = 1:length(theta)
                grad(t) = (1/m)*sum( (h-labels).*X(:,t) );
            end
        end
        
        function g = sigmoid(z)
            %This function computes the sigmoid function
            g = 1./(1+exp(-z));
        end 
    end
end